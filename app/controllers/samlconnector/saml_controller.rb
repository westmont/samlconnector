require_dependency "samlconnector/application_controller"

module Samlconnector
  class SamlController < ApplicationController
    protect_from_forgery with: :exception
    skip_before_action :verify_authenticity_token, :only => [:acs, :logout]

    def index
      @attrs = {}
    end

    def sso
      settings = Saml.get_saml_settings(Rails.configuration.sp_url_base)
      if settings.nil?
        render :action => :no_settings
        return
      end

      request = OneLogin::RubySaml::Authrequest.new
      redirect_to(request.create(settings))

    end

    def acs
      settings = Saml.get_saml_settings(Rails.configuration.sp_url_base)
      response = OneLogin::RubySaml::Response.new(params[:SAMLResponse], :settings => settings)

      if response.is_valid?
        session[:attributes] = response.attributes.attributes.each_with_object({}) do |(k,v), a|
          a[k] = (v.length == 1 ? v.first : v)
        end

        redirect_to '/'
      else
        logger.info "Response Invalid. Errors: #{response.errors}"
        @errors = response.errors
        render :action => :fail
      end
    end

    def metadata
      settings = Saml.get_saml_settings(Rails.configuration.sp_url_base)
      meta = OneLogin::RubySaml::Metadata.new
      render :xml => meta.generate(settings, true)
    end
  end
end
