module Samlconnector
  module Concerns
    module Secured
      extend ActiveSupport::Concern
      included do
        before_action :logged_in_using_saml?
      end

      def logged_in_using_saml?
        redirect_to '/saml/sso' unless session[:attributes].present?
      end
    end
  end
end
