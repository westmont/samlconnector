# module Samconnector
  class Samlconnector::Saml < ActiveRecord::Base
    def self.get_saml_settings(url_base)
      settings = OneLogin::RubySaml::Settings.new

      idp_metadata_parser = OneLogin::RubySaml::IdpMetadataParser.new

      # Returns OneLogin::RubySaml::Settings prepopulated with idp metadata
      settings = idp_metadata_parser.parse_remote(Rails.configuration.idp_metadata_url)

      # If set to false, settings will cause error
      settings.soft = true

      #SP section
      url_base ||= Rails.configuration.sp_url_base
      settings.issuer = "urn:westmont:webapps:sp"
      settings.assertion_consumer_service_url = url_base + "/saml/acs"

      settings
    end
  end
# end
